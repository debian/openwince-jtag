/*
 * $Id: buses.h,v 1.1 2003/09/05 21:09:14 telka Exp $
 *
 * Copyright (C) 2003 ETC s.r.o.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * Written by Marcel Telka <marcel@telka.sk>, 2003.
 *
 */

#ifndef BUSES_H
#define	BUSES_H

extern const bus_driver_t bcm1250_bus;
extern const bus_driver_t ixp425_bus;
extern const bus_driver_t pxa2x0_bus;
extern const bus_driver_t s3c4510_bus;
extern const bus_driver_t sa1110_bus;
extern const bus_driver_t sh7727_bus;
extern const bus_driver_t sh7750r_bus;
extern const bus_driver_t sh7751r_bus;

#endif /* BUSES_H */
